# -*- coding: utf-8 -*-
# @Time  : 2021/3/22 下午1:53
# @Author : Xinyu Zhang & Sophie How
# @FileName: Xinyu_Sophie.py
# @Software: PyCharm
import pandas as pd
import numpy as np
from pandas import DataFrame
import pickle
import collections
from statistics import median


def read_file(filepath):
    """
    Read file from filepath
    :param filepath: str
    :return: dataframe
    """
    return pd.read_csv(filepath)


def remove_id(df):
    """
    Do not use ID for cross correlation
    :param df: dataframe
    :return: cleaned data
    """
    df.drop(labels="ID", axis=1, inplace=True)
    return df


def cross_correlation(df):
    """
    Using numpy package for correlation.
    :param df: datafram without ID
    :return: None
    """
    corrcoef = np.corrcoef(df, rowvar=False)
    print(corrcoef)

    def report_a_to_d():
        """
        Print the answer from question a to d.
        :return: None
        """
        report_dic = {}
        best_attr1, best_attr2, best_corr = -float("inf"), -float("inf"), -float("inf")
        for index, attr in enumerate(corrcoef):
            curr_best_attr1, curr_best_attr2, curr_best_corr = index, -float("inf"), -float("inf")
            for value in range(len(attr)):
                if value == curr_best_attr1:
                    continue
                if abs(attr[value]) >= curr_best_corr:
                    curr_best_attr2 = value
                    curr_best_corr = abs(attr[value])
            if curr_best_corr > best_corr:
                best_attr1 = curr_best_attr1
                best_attr2 = curr_best_attr2
                best_corr = curr_best_corr
            if str(df.columns[curr_best_attr1]).strip(' ') in ['Fish', 'Meat', 'Beans']:
                report_dic[str(df.columns[curr_best_attr1]).strip(' ')] = [str(df.columns[curr_best_attr2]).strip(' '),
                                                                           curr_best_corr]
        print(f"The maximum correlation in the file is {str(df.columns[best_attr1]).strip(' ')} and "
              f"{str(df.columns[best_attr2]).strip(' ')}, with correlation of {best_corr}.")
        print(
            f"The maximum correlation of Fish is {report_dic['Fish'][0]}, with correlation of {report_dic['Fish'][1]}.")
        print(
            f"The maximum correlation of Meat is {report_dic['Meat'][0]}, with correlation of {-1*report_dic['Meat'][1]}.")
        print(
            f"The maximum correlation of Beans is {report_dic['Beans'][0]}, with correlation of {-1*report_dic['Beans'][1]}.")

    def report_e_to_f():
        """
        Print the answer of question from e to f
        :return: None
        """
        report_dic = {}
        best_attr1, best_attr2, best_corr = float("inf"), float("inf"), float("inf")
        for index, attr in enumerate(corrcoef):
            curr_best_attr1, curr_best_attr2, curr_best_corr = index, float("inf"), float("inf")
            for value in range(len(attr)):
                if value == curr_best_attr1:
                    continue
                if abs(attr[value]) <= curr_best_corr:
                    curr_best_attr2 = value
                    curr_best_corr = abs(attr[value])
            if curr_best_corr < best_corr:
                best_attr1 = curr_best_attr1
                best_attr2 = curr_best_attr2
                best_corr = curr_best_corr
            report_dic[best_attr1] = [best_attr2, best_corr]
        sort_dic = sorted(report_dic.items(), key=lambda kv:(kv[1][1], kv[0]))
        first, second = sort_dic[0][0], sort_dic[1][0]
        print(f"The first minimum correlation in the file is {str(df.columns[first]).strip(' ')} and "
              f"{str(df.columns[report_dic[8][0]]).strip(' ')}, with correlation of {report_dic[first][1]}.")
        print(f"The second minimum correlation in the file is {str(df.columns[second]).strip(' ')} and "
              f"{str(df.columns[report_dic[3][0]]).strip(' ')}, with correlation of {report_dic[second][1]}.")

    def report_g():
        """
        Print the answer of question g.
        :return: None
        """
        diff_ans = {}
        for index, attr in enumerate(corrcoef):
            max_corr, min_corr = max(attr), min(attr)
            diff = abs(max_corr-min_corr)
            diff_ans[index] = diff

        number_of_attrs = 0
        for item in sorted(diff_ans.items(), key=lambda kv:(kv[1], kv[0])):
            if number_of_attrs >= 2:
                break
            print(f"The {number_of_attrs + 1}th that deleted would be {str(df.columns[item[0]]).strip(' ')}, "
                  f"with lowest difference of max and min correlation of {item[1]}")
            number_of_attrs += 1
    report_a_to_d()
    report_e_to_f()
    report_g()

def L2_aka_euclidean_distance(x, y):
    distance = 0
    for i, j in zip(x, y):
        distance += (i - j) ** 2
    return distance ** 0.5

def distance_metric_options(method, vec1, vec2):
    # choose the usage of distance, whether Jaccard or Cosine
    if method == 'euclidean':
        distance = L2_aka_euclidean_distance(vec1, vec2)
    return distance

def implement_agglomeration(binary_matrix, new_matrix, new_df, method):
    # implement the agglomeration
    num_clusters = len(binary_matrix)  # get cluster numbers
    dist_clusters = {}  # store distances for current clusters
    merge_process = {}  # store clusters merging process
    track_smallest_cluster = []
    avg_prototype = []
    visited_pairs = []  # track visited clusters
    similar_pair = (0, 1)  # initial clusters prepare to merge
    all_clusters_id = [i for i in range(num_clusters)]  
    cnt = 0
    while num_clusters > 1:
        cnt += 1
        print("cnt ", cnt)
        for i in range(0, num_clusters):
            for j in range(i+1, num_clusters):
                if (i, j) not in visited_pairs:
                    dist_val = distance_metric_options(method, binary_matrix[i], binary_matrix[j])  # calculate distance between two clusters
                    dist_clusters[(i, j)] = dist_val  # store distance
                    visited_pairs.append((i, j))  # check the pair of clusters
                minimum_distance = min(dist_clusters.values())  # get the minimum distance inside all clusters
                similar_pair = min(dist_clusters, key=dist_clusters.get) # get the two clusters id
        merge_process[similar_pair] = minimum_distance   
        # merge two similar clusters
        cluster_id1 = similar_pair[0]  # get first cluster
        cluster_id2 = similar_pair[1]   # get second cluster
        # print('cluster_id1= ',cluster_id1)
        # print('cluster_id2= ', cluster_id2)

        size_cluster_id1 = len(str(cluster_id1).split(',')) if ',' in str(cluster_id1) else 1   # get size of cluster_id1
        size_cluster_id2 = len(str(cluster_id2).split(',')) if ',' in str(cluster_id2) else 1   # get size of cluster_id2

        if size_cluster_id1 < size_cluster_id2:
            track_smallest_cluster.append(cluster_id1)  # track the smallest cluster
        else:
            track_smallest_cluster.append(cluster_id2)
        merging_pair = (cluster_id1, cluster_id2) if (cluster_id1, cluster_id2) in dist_clusters else (cluster_id2, cluster_id1) # pair is (x,y) or (y,x)
        new_matrix[merging_pair] = [ (a+b)/2 for a,b in zip(new_matrix.get(cluster_id1), new_matrix.get(cluster_id2))] # updating jaccard matrix by median method
        del new_matrix[cluster_id1]  # delete 2 old clusters in the updating matrix
        del new_matrix[cluster_id2]
        
        del dist_clusters[merging_pair]   # remove the distance between two merging clusters in the current clusters
        cluster_members = flatten(similar_pair)  # get all memebers in side the cluster 
        new_clusters = [x for x in all_clusters_id if x not in cluster_members]  
        new_clusters.append((merging_pair)) # append the new cluster inside the non_members
        if cluster_id1 in new_clusters:
            new_clusters.remove(cluster_id1)  
        if cluster_id2 in new_clusters:
            new_clusters.remove(cluster_id2)
        for k in new_clusters:
            pair1 = (k, cluster_id1) if (k, cluster_id1) in dist_clusters else (cluster_id1, k)  # pair(x,y) or (y,x)
            pair2 = (k, cluster_id2) if (k, cluster_id2) in dist_clusters else (cluster_id2, k) # pair(x,y) or (y,x)
            if (pair1 and pair2) in dist_clusters:
                median_center = median([dist_clusters.get(pair1), dist_clusters.get(pair2)])
                avg_prototype.append(median_center)
                dist_clusters[(k, (cluster_id1, cluster_id2))] = median_center # get distance for new cluster by center linkage              
                del dist_clusters[pair1]  # delete old two clusters with distances inside current clusters.
                del dist_clusters[pair2]
            all_clusters_id = new_clusters
        num_clusters -= 1
    print("merge_process = \n", merge_process)
    print('The Last 20 Merged Smallest Clusters are: \n', track_smallest_cluster[-20:])
    print("The Last Six Clusters: Average Prototype = \n", avg_prototype[-6:])

    
def flatten(similar_pair):
    # faltten the data
    if isinstance(similar_pair, collections.abc.Iterable):
        return [each_cluster for clusters in similar_pair for each_cluster in flatten(clusters)]
    else:
        return [similar_pair]


if __name__ == '__main__':
    df = read_file("../PCA_SHOPPING_CART.csv")
    cleaned_df = remove_id(df)
    # cleaned_df = cleaned_df.head(50)
    cross_correlation(cleaned_df)
    # convert dataset to matrix
    # matrix = cleaned_df.to_numpy(dtype='int') # convert dataframe to vector
    # # print(matrix)
    # # create new matrix to update binary matrix
    # dynamic_matrix = {}
    # for i in range(len(matrix)):
    #     dynamic_matrix[i] = matrix[i]
    # # print(dynamic_matrix)
    # implement_agglomeration(matrix, dynamic_matrix, cleaned_df, 'euclidean')







# def agglomeration(df):
#     """
#     Implement agglomeration
#     :param df: binary data frame
#     :return: None
#     """
#     all_cluster = {}
#     smaller_cluster = []
#     for index in range(df.shape[0]):
#         all_cluster[index] = df.loc[index].values
#     distances = [[0 for _ in range(len(all_cluster.keys()))] for _ in range(len(all_cluster.keys()))]
#     distance_df = DataFrame(distances, columns=[str(index) for index in range(len(all_cluster))])
#     clusters = []
#     while len(list(all_cluster)) != 1:
#         # find closest clusters
#         current_select_1, current_select_2 = 0, 0
#         current_distance = float('inf')
#         for slow in range(len(all_cluster)):
#             for fast in range(slow + 1, len(all_cluster)):
#                 current_label_1, current_data_1 = slow, all_cluster[slow]
#                 current_label_2, current_data_2 = fast, all_cluster[fast]
#                 distance = L2_aka_euclidean_distance(current_data_1, current_data_2)
#                 distance_df.iloc[slow, fast] = distance
#                 if distance <= current_distance:
#                     current_select_1, current_select_2 = current_label_1, current_label_2
#                     current_distance = distance
#         # merging
#         smaller_cluster.append(all_cluster[current_select_1]
#                                if len(all_cluster[current_select_1]) < len(all_cluster[current_select_2])
#                                else all_cluster[current_select_2])
#         all_cluster[current_select_1] = merge(all_cluster[current_select_1], all_cluster[current_select_2])
#         del all_cluster[current_select_2]
#         for row in range(distance_df.shape[0]):
#             distance_df.iloc[row, current_select_1] = (distance_df.iloc[row, current_select_1] + distance_df.iloc[row, current_select_2]) / 2
#         new_cluster = f"({distance_df.columns[current_select_1]},{distance_df.columns[current_select_2]})"
#         distance_df.rename(columns={f"{distance_df.columns[current_select_1]}": new_cluster}, inplace=True)
#         min_cluster_in_new_cluster = sorted(new_cluster.replace("(", "").replace(")", "").split(","),
#                                             key=lambda x: int(x) - 0)[0]
#         clusters.append(min_cluster_in_new_cluster)
#         distance_df = distance_df.drop(labels=distance_df.columns[current_select_2], axis=1)
#         temp_dic = {}
#         for index, value in all_cluster.items():
#             if index > current_select_2:
#                 temp_dic[index - 1] = value
#             else:
#                 temp_dic[index] = value
#         all_cluster = temp_dic
#         if len(all_cluster) == 6:
#             pickle.dump(all_cluster, open("prototype.p", 'wb'))
#             pickle.dump(distance_df, open("clusters.p", 'wb'))
#         print(len(all_cluster))
#     pickle.dump(smaller_cluster, open("smaller_cluster.p", 'wb'))
#     print('Finished')


# def merge(x, y):
#     """
#     Merge the data in selected two clusters
#     :param x: cluster A
#     :param y: cluster B
#     :return: New merged data
#     """
#     new_list = []
#     for value_1, value_2 in zip(x, y):
#         new_list.append((value_1 + value_2) / 2)
#     return new_list


